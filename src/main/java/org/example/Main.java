package org.example;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._helpers.esql.objects.ObjectsEsqlAdapter;
import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.elasticsearch.indices.DeleteAliasRequest;
import co.elastic.clients.elasticsearch.indices.GetAliasRequest;
import co.elastic.clients.elasticsearch.indices.GetIndexRequest;
import co.elastic.clients.elasticsearch.indices.IndexSettings;
import co.elastic.clients.elasticsearch.indices.PutAliasRequest;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import net.datafaker.Faker;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;

public class Main {

    private static final String ELASTIC_SERVER_1_URL = "https://elastic1.echothree.com:9200";
    private static final String ELASTIC_SERVER_2_URL = "https://elastic2.echothree.com:9200";
    private static final String ELASTIC_SERVER_3_URL = "https://elastic3.echothree.com:9200";
    private static String ELASTIC_API_KEY;

    private static final Faker FAKER = new Faker();

    // Elastic Common Schema: https://www.elastic.co/guide/en/ecs/current/ecs-reference.html

    public static void main(String[] args)
            throws Exception {
        var context = parseCommandLine(args);

        initElasticApiKey();

        System.out.println(context.optionA());
        System.out.println(context.optionB());

        performFileTest();

        performElasticTest();
    }

    public static void initElasticApiKey()
            throws Exception {
        ELASTIC_API_KEY = System.getenv("ELASTIC_API_KEY");

        if (ELASTIC_API_KEY == null) {
            throw new Exception("ELASTIC_API_KEY not set in environment variables");
        }
    }

    private static void performFileTest()
            throws IOException {
        try(var lines = Files.lines(Path.of("/Users/rich/test.txt"))) {
            lines.forEach(System.out::println);
        }
    }

    private static Context parseCommandLine(String[] args)
            throws ParseException {
        var cmd = new DefaultParser()
                .parse(new Options()
                        .addOption(Option.builder().option("a").longOpt("optionA").hasArg().desc("option a").build())
                        .addOption(Option.builder().option("b").longOpt("optionB").hasArg().desc("option b").build()), args);

        String optionA = cmd.getOptionValue("a");
        String optionB = cmd.getOptionValue("b");

        return new Context(optionA, optionB);
    }

    private static void performElasticTest()
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        // Based on: https://stackoverflow.com/questions/64980339/disabling-ssl-verification-for-elastic-search-restclient-not-working-in-java
        final var sslBuilder = SSLContexts.custom()
                .loadTrustMaterial(null, (x509Certificates, s) -> true);
        final var sslContext = sslBuilder.build();

        // Based on: https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/current/getting-started-java.html
        // Create the low-level client
        try(final var restClient = RestClient
                .builder(HttpHost.create(ELASTIC_SERVER_1_URL), HttpHost.create(ELASTIC_SERVER_2_URL), HttpHost.create(ELASTIC_SERVER_3_URL))
                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                    @Override
                    public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                        return httpClientBuilder
                                .setSSLContext(sslContext)
                                .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE);
                    }
                })
                .setDefaultHeaders(new Header[]{
                        new BasicHeader("Authorization", "ApiKey " + ELASTIC_API_KEY)
                })
                .build()) {
            try(final var transport = new RestClientTransport(restClient, new JacksonJsonpMapper())) {
                performActions(transport);
            }
        }
    }

    private static void performActions(final RestClientTransport transport)
            throws IOException {
        final ElasticsearchClient client = new ElasticsearchClient(transport);
        final var start = Instant.now();

        getIndex(client);

        createIndex(client);
        createAlias(client);

        getIndex(client);

        getAlias(client);
//
//        addBulkProductsToIndex(client);
//
////        addManyProductsToIndex(client);
//
//        addProductToIndex(client);
//        client.indices().refresh();
//
//        searchForProduct(client);
//
//        updateProductInIndex(client);
//        client.indices().refresh();
//
//        searchForProduct(client);
//
//        deleteProductFromIndex(client);

        deleteAlias(client);
        deleteIndex(client);

        var end = Instant.now();
        System.out.println("Finished in: " + Duration.between(start, end).toMillis() + " ms\n");
    }

    private static void createIndex(final ElasticsearchClient client)
            throws IOException {
        client.indices().create(c -> c
                .index("products-20240630")
                .settings(new IndexSettings.Builder()
                        .numberOfShards("6")
                        .numberOfReplicas("1")
                        .build())
                .mappings(mp -> mp
                        .properties("sku", p -> p.keyword(k -> k))
                        .properties("name", p -> p.text(t -> t))
                        .properties("price", p -> p.double_(d -> d))
                        .properties("superDescription", p -> p.text(t -> t))
                        .properties("releaseDate", p -> p.date(d -> d))
                        .properties("tags", p -> p.keyword(k -> k))
                )
        );
    }

    private static void getIndex(final ElasticsearchClient client)
            throws IOException {
        try {
            var response = client.indices().get(new GetIndexRequest.Builder()
                    .index("products-20240630")
                    .build());

            System.out.println(response.result());
            System.out.println(response.get("products-20240630"));
        } catch (ElasticsearchException ee) {
            System.out.println("no such index");
        }
    }

    private static void getAlias(final ElasticsearchClient client)
            throws IOException {
        try {
            var response = client.indices().getAlias(new GetAliasRequest.Builder()
                    .name("products")
                    .build());

            // {products-20240630=IndexAliases: {"aliases":{"products":{}}}}
            System.out.println(response.result());
            // IndexAliases: {"aliases":{"products":{}}}
            System.out.println(response.get("products-20240630"));
        } catch (ElasticsearchException ee) {
            System.out.println("no such alias");
        }
    }

    private static void createAlias(final ElasticsearchClient client)
            throws IOException {
        client.indices().putAlias(new PutAliasRequest.Builder()
                .index("products-*")
                .name("products")
                .build());
    }

    private static void deleteIndex(final ElasticsearchClient client)
            throws IOException {
        client.indices().delete(d -> d
                .index("products-20240630")
        );
    }

    private static void deleteAlias(final ElasticsearchClient client)
            throws IOException {
        client.indices().deleteAlias(new DeleteAliasRequest.Builder()
                .index("products-20240630")
                .name("products")
                .build());
    }

    private static void deleteProductFromIndex(final ElasticsearchClient client)
            throws IOException {
        client.delete(d -> d.index("products").id("bk-1"));
    }

    private static void updateProductInIndex(final ElasticsearchClient client)
            throws IOException {
        var product = new Product("bk-1", "Country bike", 123.0, "This is an awesome country bike", "2015-01-01T12:10:30.123456789Z", List.of("tag1", "tag2"));

        client.update(u -> u
                        .index("products")
                        .id(product.sku())
                        .doc(product)
                        .docAsUpsert(true),
                Product.class
        );
    }

    private static void searchForProduct(final ElasticsearchClient client)
            throws IOException {

        String queryProduct =
                """
                    from products
                    | where sku == "bk-1"
                    | limit 10
                """;

        var products = client.esql().query(ObjectsEsqlAdapter.of(Product.class), queryProduct);

        for (var product : products) {
            System.out.println("Product name: " + product.name());
            System.out.println("Product description: " + product.description());
        }
    }

    private static void addProductToIndex(final ElasticsearchClient client)
            throws IOException {
        var product = new Product("bk-1", "City bike", 123.0, "This is an awesome city bike", "2015-01-01T12:10:30.123456789Z", List.of("tag1", "tag2"));

        var response = client.index(i -> i
                .index("products")
                .id(product.sku())
                .document(product)
        );

        System.out.println("Indexed with version " + response.version());
    }

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

    // Source: https://stackoverflow.com/questions/41427384/how-to-get-default-zoneoffset-in-java-8
//    private static final LocalDate ld = LocalDate.of( 2017 , 12 , 25 );
//    private static final ZonedDateTime zdtXmas = ld.atStartOfDay( z );
//    private static final ZoneOffset zoneOffsetXmas = zdtXmas.getOffset();

    private static final ZoneId zoneId = ZoneId.of( "America/Chicago" );
    private static final ZoneOffset zoneOffset = zoneId.getRules().getOffset(Instant.now()); // Should be for date/time we're importing. :(

    private static void addManyProductsToIndex(final ElasticsearchClient client)
            throws IOException {
        for(int j = 0; j < 1500000; j++) {
            var dateTime = LocalDateTime.now().atOffset(zoneOffset);
            var dateTimeText = dateTime.format(formatter);

            var product = new Product(UUID.randomUUID().toString(), "Many products example", 123.0, "This is an awesome product", dateTimeText, List.of("tag1", "tag2"));

            var response = client.index(i -> i
                    .index("products")
                    .id(product.sku())
                    .document(product)
            );

            if(j % 10000 == 0) {
                System.out.println("Indexed " + product.sku() + " with version " + response.version());
            }
        }
    }

    private static void addBulkProductsToIndex(final ElasticsearchClient client)
            throws IOException {
        BulkRequest.Builder br = null;

        for(int j = 0; j < 1500000; j++) {
            if(j % 1000 == 0) {
                if(br != null) {
                    executeBulkRequest(client, br);
                }

                br = new BulkRequest.Builder();
            }

            var dateTime = LocalDateTime.now().atOffset(zoneOffset);
            var dateTimeText = dateTime.format(formatter);

            var product = new Product(
                    UUID.randomUUID().toString(),
                    FAKER.commerce().productName(),
                    Float.valueOf(FAKER.commerce().price()),
                    "This is an awesome product",
                    dateTimeText,
                    List.of("tag1", "tag2"));

            br.operations(op -> op
                    .index(idx -> idx
                            .index("products")
                            .id(product.sku())
                            .document(product)
                    )
            );
        }

        executeBulkRequest(client, br);
    }

    private static void executeBulkRequest(final ElasticsearchClient client, final BulkRequest.Builder br)
            throws IOException {
        var bulkRequest = br.build();

        if(!bulkRequest.operations().isEmpty()) {
            var result = client.bulk(bulkRequest);

            // Log errors, if any
            if(result.errors()) {
                System.out.println("Bulk had errors");

                for(BulkResponseItem item : result.items()) {
                    if(item.error() != null) {
                        System.out.println(item.error().reason());
                    }
                }
            }
        }
    }

}
