package org.example.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Numbers {
    private static final int BOUND = 10;
    private static final int N_PRODUCERS = 4;
    private static final int N_CONSUMERS = Runtime.getRuntime().availableProcessors();
    private static final int poisonPill = Integer.MAX_VALUE;
    private static final int poisonPillPerProducer = N_CONSUMERS / N_PRODUCERS;
    private static final int mod = N_CONSUMERS % N_PRODUCERS;

    // Based on: https://www.baeldung.com/java-blocking-queue
    public static void main(String[] args) {
        BlockingQueue<Integer> queue = new LinkedBlockingQueue<>(BOUND);

        for (int i = 1; i < N_PRODUCERS; i++) {
            new Thread(new NumbersProducer(queue, poisonPill, poisonPillPerProducer)).start();
        }

        for (int j = 0; j < N_CONSUMERS; j++) {
            new Thread(new NumbersConsumer(queue, poisonPill)).start();
        }

        new Thread(new NumbersProducer(queue, poisonPill, poisonPillPerProducer + mod)).start();
    }

}
