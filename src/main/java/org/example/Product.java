package org.example;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

@JsonIgnoreProperties({ "name.keyword", "sku.keyword", "superDescription.keyword" })
public record Product(
        String sku,
        String name,
        double price,
        @JsonProperty("superDescription")
        String description,
        String releaseDate,
        List<String> tags
) {
}
